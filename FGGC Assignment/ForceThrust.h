#pragma once
#include "ForceGenerator.h"

class ForceThrust : public ForceGenerator
{
public:
	ForceThrust(const Vector3& force) : _force(force) {};
	~ForceThrust();

	void updateForce(GameObject* go) override;

	void SetForce(Vector3 force) { _force = force; };

private:
	Vector3 _force;
};


