#pragma once
#include "Component.h"

class ParticleData : public Component
{
public:
	ParticleData();
	~ParticleData();

	float _aliveTime;

	void ChangeTime(float amount) { _aliveTime += amount; }

private:

};


