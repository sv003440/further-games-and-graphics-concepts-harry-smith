#include "Vector3.h"

Vector3::~Vector3()
{

}

void Vector3::Normalize()
{
	float mag = GetMagnitude();

	x /= mag;
	y /= mag;
	z /= mag;

}

Vector3 Vector3::operator*(const float amount)
{
	return Vector3(x*amount, y*amount, z*amount);
}

void Vector3::operator*=(const float amount)
{
	x *= amount;
	y *= amount;
	z *= amount;
}

Vector3 Vector3::operator/(const float amount)
{
	return Vector3(x/amount, y/amount, z/amount);
}

void Vector3::operator/=(const float amount)
{
	x /= amount;
	y /= amount;
	z /= amount;
}

float Vector3::operator*(const Vector3 &v2) const//dot product
{
	return x*v2.x + y*v2.y + z*v2.z;
	//returns  |v1||v2|cos(angle)
}

Vector3 Vector3::operator/(const Vector3 &v2) //cross product
{
	float newX = y*v2.z - z*v2.y;
	float newY = z*v2.x - x*v2.z;
	float newZ = x*v2.y - y*v2.x;

	return Vector3(newX, newY, newZ);
}

Vector3 Vector3::operator+(const Vector3 &v2)
{
	return Vector3(x+v2.x, y+v2.y, z+v2.z);
}

void Vector3::operator+=(const Vector3 &v2)
{
	x += v2.x;
	y += v2.y;
	z += v2.z;
}

Vector3 Vector3::operator-(const Vector3 &v2)
{
	return Vector3(x - v2.x, y - v2.y, z - v2.z);
}

void Vector3::operator-=(const Vector3 &v2)
{
	x -= v2.x;
	y -= v2.y;
	z -= v2.z;
}

bool Vector3::operator<(const Vector3 &v2) const
{
	if (GetMagnitudeSquared() < v2.GetMagnitudeSquared())
		return true;
	else
		return false;
}

bool Vector3::operator<=(const Vector3 &v2) const
{
	if (GetMagnitudeSquared() <= v2.GetMagnitudeSquared())
		return true;
	else
		return false;
}

bool Vector3::operator>(const Vector3 &v2) const
{
	if (GetMagnitudeSquared() > v2.GetMagnitudeSquared())
		return true;
	else
		return false;
}

bool Vector3::operator>=(const Vector3 &v2) const
{
	if (GetMagnitudeSquared() >= v2.GetMagnitudeSquared())
		return true;
	else
		return false;
}