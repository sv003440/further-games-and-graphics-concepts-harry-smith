#include "Appearence.h"

Appearence::Appearence(Geometry geo, Material mat) : _geometry(geo), _material(mat), _textureRV(nullptr)
{
	Component::_componentType = APPEARENCE;
};

Appearence::~Appearence()
{
}

void Appearence::RecieveMsg(Message message, Vector3 amount)
{
	switch (message)
	{
		//case
	case ADDFORCE: break;
	default: ;
	}
}

void Appearence::Draw(ID3D11DeviceContext * pImmediateContext)
{
	// NOTE: We are assuming that the constant buffers and all other draw setup has already taken place

	// Set vertex and index buffers
	pImmediateContext->IASetVertexBuffers(0, 1, &_geometry.vertexBuffer, &_geometry.vertexBufferStride, &_geometry.vertexBufferOffset);
	pImmediateContext->IASetIndexBuffer(_geometry.indexBuffer, DXGI_FORMAT_R16_UINT, 0);

	pImmediateContext->DrawIndexed(_geometry.numberOfIndices, 0, 0);
}
