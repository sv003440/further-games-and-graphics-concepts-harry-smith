#pragma once
#include "Vector3.h"

class GameObject;

enum ComponentType
{
	NONE,
	TRANSFORM,
	APPEARENCE,
	PHYSICS,
	BOUNDINGSPHERE,
	PARTICLEDATA
};

enum Message
{
	ADDFORCE,
	COLLIDEDWITH,
	GROUNDED,
};

class Component
{
public:
	Component() : _componentType(NONE), _parent(nullptr){};
	virtual ~Component();

	virtual void Update(float deltaTime);

	ComponentType GetType() const { return _componentType; };

	virtual void RecieveMsg(Message message, Vector3 amount = Vector3()) = 0;

	void SetParent(GameObject& parentIn) { _parent = &parentIn; };

protected:
	ComponentType _componentType;

	GameObject* _parent;

};


