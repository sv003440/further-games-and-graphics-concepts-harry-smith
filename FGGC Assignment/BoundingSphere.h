#pragma once
#include "Component.h"

class BoundingSphere : public Component
{
public:
	BoundingSphere(float radius = 0.6f);
	~BoundingSphere();

	void Update(float deltaTime) override;

	void RecieveMsg(Message message, Vector3 amount = Vector3());

	bool CollisionCheck(Vector3 otherPosition, float radius);

	bool CollisionCheck(BoundingSphere* otherSphere);

	bool GetColliding() const { return _isColliding; };
	void SetColliding(bool setColliding) { _isColliding = setColliding; };

	float GetRadius() const { return  _radius; };
	void SetRadius(float newRadius) { _radius = newRadius; };

	Vector3 GetPosition();

	GameObject* GetParent() { return _parent; };

private:
	float _radius;
	bool _isColliding;
};


