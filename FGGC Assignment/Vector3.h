#pragma once
#include<math.h>

class Vector3
{
private:


public:
	float x, y, z; 

	Vector3() :x(0), y(0), z(0){}
	Vector3(float x, float y, float z) : x(x), y(y), z(z){}
	Vector3(const Vector3& other) :x(other.x), y(other.y), z(other.z){}
	~Vector3();

	float GetMagnitudeSquared() const { return x*x + y*y + z*z; }
	float GetMagnitude() const { return sqrtf(GetMagnitudeSquared()); }

	void Normalize();

	Vector3 operator*(const float amount);
	void operator*=(const float amount);

	Vector3 operator/(const float amount);
	void operator/=(const float amount);

	float operator*(const Vector3& v2) const; //dot product
	Vector3 operator/(const Vector3& v2); //cross product
	Vector3 operator+(const Vector3& v2);
	void operator+=(const Vector3& v2);

	Vector3 operator-(const Vector3& v2);
	void operator-=(const Vector3& v2);

	bool operator<(const Vector3& v2) const;
	bool operator<=(const Vector3& v2) const;
	bool operator>(const Vector3& v2) const;
	bool operator>=(const Vector3& v2) const;
};