#include "GameObject.h"

GameObject::GameObject(string type, initializer_list<Component*> componentList) : _objectType(type), _parent(nullptr), _components(componentList)
{
	for (Component* comp : _components)
	{
		if (comp)
			comp->SetParent(*this);
	}
}

GameObject::~GameObject()
{
	//for (Component* comp : _components)
	//{
	//	if (comp)
	//	{
	//		delete comp;
	//	}
	//		
	//}
}

void GameObject::Update(float t)
{
	// Calculate world orientationMatrix
	Transform* _transform = GetComponent<Transform*>(TRANSFORM);
	XMMATRIX scale = XMMatrixScaling(_transform->GetScale().x, _transform->GetScale().y, _transform->GetScale().z);

	XMMATRIX orientationMatrix;
	Quaternion rotation = _transform->GetOrientation();
	rotation.normalise();

	CalculateTransformMatrix(orientationMatrix, _transform->GetPosition(), rotation);

	XMStoreFloat4x4(&_world, scale * orientationMatrix);

	if (_parent)
	{
		XMStoreFloat4x4(&_world, this->GetWorldMatrix() * _parent->GetWorldMatrix());
	}

	for (Component* comp : _components)
	{
		if (comp)
			comp->Update(t);
	}
}

void GameObject::SendMsg(Message message, Vector3 amount)
{
	for (auto comp : _components)
	{
		comp->RecieveMsg(message, amount);
	}
}

void GameObject::AddComponent(Component* newComponent)
{
	_components.push_back(newComponent);

	newComponent->SetParent(*this);
}