#include "Physics.h"
#include "GameObject.h"


Physics::Physics() : _velocity(Vector3()), _acceleration(Vector3()), _netForce(Vector3()), _mass(1.0f),
					_damping(0.01f), _coefRestitution(0.1f), _isColliding(false), _collisionVelocity(Vector3()), 
					_orientation(Quaternion()), _momentOfInertia(Vector3())
{
	Component::_componentType = PHYSICS;
}

Physics::Physics(Vector3 vel, Vector3 acc) : _velocity(vel), _acceleration(acc), _netForce(Vector3()), _mass(1.0f), 
											_damping(0.01f), _coefRestitution(0.1f), _isColliding(false), _collisionVelocity(Vector3()), 
											_orientation(Quaternion()), _momentOfInertia(Vector3()) 
{
	Component::_componentType = PHYSICS;
}


Physics::~Physics()
{

}

void Physics::Update(float deltaTime)
{
	UpdateAcceleration();
	Move(deltaTime);
	_netForce = Vector3(); //reset net force
}

void Physics::Move(float deltaTime)
{
	Transform* parentTransform = _parent->GetComponent<Transform*>(TRANSFORM);
	Vector3 pos = parentTransform->GetPosition();

	Vector3 previousPos = pos;

	if (_isColliding)
	{
		_velocity = _collisionVelocity;
	}

	pos += _velocity * deltaTime + _acceleration * 0.5 * deltaTime * deltaTime;

	if (pos.y < _groundHeight) //to stop them pushing through the floor
	{
		pos.y = _groundHeight;
	}

	parentTransform->SetPosition(pos);

	_velocity += _acceleration * deltaTime;

	_isColliding = false;

	//_damping hard coded
	if (_damping == 0)
	{
		return;
	}
	_velocity *= pow(_damping, deltaTime);
}

void Physics::UpdateAcceleration()
{
	if (_mass == 0)
		return;
	_acceleration = _netForce / _mass;
}

void Physics::RecieveMsg(Message message, Vector3 amount)
{
	switch (message)
	{
	case Message::ADDFORCE:
		AddForce(amount);
		break;
	case COLLIDEDWITH:
		//do something?
	case GROUNDED:

		break;
	}
}

void Physics::CollisionResolution(Physics* physics1, Physics* physics2)
{
	physics1->SetColliding(true);
	physics2->SetColliding(true);

	Vector3 velocity1 = physics1->GetVelocity();
	auto mass1 = physics1->GetMass();

	Vector3 velocity2 = physics2->GetVelocity();
	auto mass2 = physics2->GetMass();
	auto totalMass = mass1 + mass2;

	auto newVelocity1 =	
			(velocity1 * mass1
			+ velocity2 * mass2
			+ (velocity2 - velocity1) * mass2 * physics1->GetCoefRestitution())
			/ totalMass;

	auto newVelocity2 =
			(velocity2 * mass2
			+ velocity1 * mass1
			+ (velocity1 - velocity2) * mass1 * physics2->GetCoefRestitution())
		/ totalMass;


	physics1->SetCollisionVelocity(newVelocity1);
	physics2->SetCollisionVelocity(newVelocity2);
}

void Physics::CalculateMomentOfInertia()
{
	_momentOfInertia.x = (_mass / 12)*(_depth*_depth + _height*_height);
	_momentOfInertia.y = (_mass / 12)*(_width*_width + _height*_height);
	_momentOfInertia.z = (_mass / 12)*(_width*_width + _depth*_depth);
}



