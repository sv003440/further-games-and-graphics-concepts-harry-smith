#include "BoundingSphere.h"
#include "GameObject.h"

BoundingSphere::BoundingSphere(float radius) : _radius(radius), _isColliding(false)
{
	_componentType = BOUNDINGSPHERE;
}

BoundingSphere::~BoundingSphere()
{

}

void BoundingSphere::RecieveMsg(Message message, Vector3 amount)
{
	//switch (message)
	//{
	//case //something:
	//	break;
	//default:
	//	break;
	//}
}

bool BoundingSphere::CollisionCheck(Vector3 otherPosition, float radius)
{
	Vector3 myPosition = _parent->GetComponent<Transform*>(TRANSFORM)->GetPosition();

	float combinedRadiusSqrd = (radius + _radius) * (radius + _radius);

	Vector3 sourceToDest = myPosition - otherPosition;

	if (sourceToDest.GetMagnitudeSquared() < combinedRadiusSqrd)
	{
		_isColliding = true;
		return true;
	}
		
	return false;
}

bool BoundingSphere::CollisionCheck(BoundingSphere* otherSphere)
{
	auto myPosition = GetPosition();
	auto otherPosition = otherSphere->GetPosition();

	auto otherRadius = otherSphere->GetRadius();

	auto combinedRadiusSqrd = (otherRadius + _radius) * (otherRadius + _radius);

	auto sourceToDest = myPosition - otherPosition;

	auto magSquared = sourceToDest.GetMagnitudeSquared();

	if (magSquared == 0.0f)
		return false; //doesnt matter what it return, im not using it at the moment

	if (magSquared < combinedRadiusSqrd)
	{
		Physics::CollisionResolution(_parent->GetComponent<Physics*>(PHYSICS), otherSphere->GetParent()->GetComponent<Physics*>(PHYSICS));

		_isColliding = true;
		return true;
	}
	_isColliding = false;
	return false;
}

Vector3 BoundingSphere::GetPosition()
{
	return _parent->GetComponent<Transform*>(TRANSFORM)->GetPosition();
}


void BoundingSphere::Update(float deltaTime)
{

}


