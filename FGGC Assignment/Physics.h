#pragma once
#include "Component.h"
#include "Quaternion.h"

class Physics : public Component
{
public:
	Physics();
	Physics(Vector3 vel, Vector3 acc);

	~Physics();

	Vector3 GetVelocity() const { return _velocity; };
	Vector3 GetAcceleration() const { return _acceleration; };

	float GetMass() const { return _mass; };
	Vector3 GetNetForce() const { return _netForce; };

	float GetDrag() const { return _damping; };

	void SetVelocity(Vector3 vel) { _velocity = vel; };
	void SetAcceleration(Vector3 acc) { _acceleration = acc; };

	void SetDamping(float newDamping) { _damping = newDamping; };
	void SetMass(float newMass) { _mass = newMass; };

	void SetCoefRestitution(float newCoef) { _coefRestitution = newCoef; };
	float GetCoefRestitution() const { return _coefRestitution; };

	void Update(float deltaTime) override;

	void AddForce(Vector3 force) { _netForce += force; };
	void UpdateAcceleration();

	void RecieveMsg(Message message, Vector3 amount = Vector3()) override;

	void SetColliding(bool isColl) { _isColliding = isColl; };
	bool GetColliding() const { return _isColliding; };

	static void CollisionResolution(Physics* physics1, Physics* physics2);

	void SetCollisionVelocity(Vector3 newVelocity) { _collisionVelocity = newVelocity; };

	Vector3 GetMomentOfInertia() const { return _momentOfInertia; };

	void CalculateMomentOfInertia();

private:
	Vector3 _velocity;
	Vector3 _acceleration;

	Vector3 _collisionVelocity;

	Vector3 _netForce;

	float _mass;
	float _damping;

	float _coefRestitution;
	bool _isColliding;

	float _groundHeight = 0.5f;

	void Move(float deltaTime);

	Quaternion _orientation;
	float _height;
	float _width;//for orientation purposes, everything is pretending to be a cuboid
	float _depth;
	Vector3 _momentOfInertia; //should be a 3x3 matrix?
};


