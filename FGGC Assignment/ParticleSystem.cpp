#include "ParticleSystem.h"

ParticleSystem::ParticleSystem()
{
	
}

ParticleSystem::ParticleSystem(std::initializer_list<GameObject*> repoIn, Vector3 initThrust) : _repository(repoIn)
{
	//ParticleSystem();
	_thrust = new ForceThrust(Vector3());
}

ParticleSystem::~ParticleSystem()
{
	for (GameObject* go : _repository)
	{
		if (go)
		{
			delete go;
		}
	}
}

void ParticleSystem::Update(float deltaTime)
{
	for (GameObject* go : _repository)
	{
		if (!IsObjectDead(go, deltaTime))
		{
			_thrust->updateForce(go);
			go->Update(deltaTime);
		}
	}
}

void ParticleSystem::Draw(ID3D11DeviceContext* pImmediateContext)
{
	for (GameObject* go : _repository)
	{
		if (go)
		{
			go->GetComponent<Appearence*>(APPEARENCE)->Draw(pImmediateContext);
		}
	}
}

bool ParticleSystem::IsObjectDead(GameObject* go, float deltaTime)
{
	if (go->GetComponent<ParticleData*>(PARTICLEDATA)->_aliveTime > 0)
	{
		go->GetComponent<ParticleData*>(PARTICLEDATA)->ChangeTime(deltaTime);
		return false;
	}

	return true;
}