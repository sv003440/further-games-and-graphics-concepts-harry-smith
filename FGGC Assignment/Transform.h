//#include"Vector3.h"
#pragma once

#include "Component.h"
#include "Quaternion.h"

class Transform : public Component
{
public:
	Transform();
	Transform(Vector3 pos, Quaternion orient, Vector3 scale) { _position = pos, _orientation = orient, _scale = scale; };
	~Transform();

	void SetPosition(Vector3 newPosition){ _position = newPosition; };
	void SetPosition(float x, float y, float z) { SetPosition(Vector3(x, y, z)); }

	void SetOrientation(Quaternion newOrientation) { _orientation = newOrientation; };

	void SetScale(Vector3 newScale){ _scale = newScale; };
	void SetScale(float x, float y, float z) { SetScale(Vector3(x, y, z)); };

	Vector3 GetPosition()const { return _position; };

	Quaternion GetOrientation() const { return _orientation; };
	Vector3 GetScale()const { return _scale; };

	void RecieveMsg(Message message, Vector3 amount = Vector3());

	void Update(float deltaTime) override; 

private:
	Vector3 _previousPosition;
	Vector3 _position;
	Vector3 _scale;
	Quaternion _orientation;
};