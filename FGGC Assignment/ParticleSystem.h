#pragma once
#include <vector>
#include <directxmath.h>
#include <d3d11_1.h>
#include "GameObject.h"
#include "ForceThrust.h"

using namespace DirectX;
//incomplete
//see tutorial 4

class ParticleSystem
{
public:
	ParticleSystem();
	ParticleSystem(std::initializer_list<GameObject*> repoIn, Vector3 initThrust = Vector3(1.0f, 0.0f, 0.0f));
	~ParticleSystem();

	void Update(float deltaTime);
	void Draw(ID3D11DeviceContext* pImmediateContext);

private:
	int size;
	std::vector<GameObject*> _repository;

	ForceThrust* _thrust;

	bool IsObjectDead(GameObject* go, float deltaTime);

		
};

