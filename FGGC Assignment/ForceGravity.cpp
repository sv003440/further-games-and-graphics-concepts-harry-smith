#include "ForceGravity.h"

ForceGravity::~ForceGravity()
{
}

void ForceGravity::updateForce(GameObject* go)
{
	Physics* phys = go->GetComponent<Physics*>(PHYSICS);

	float mass = phys->GetMass();
	if (mass == 0)
		return;

	phys->AddForce(_gravity * mass);
}