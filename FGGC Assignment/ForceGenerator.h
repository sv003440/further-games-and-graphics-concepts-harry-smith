#pragma once
#include"GameObject.h"

class ForceGenerator
{
public:
	virtual ~ForceGenerator() = default; //added with resharper

	virtual void updateForce(GameObject* go) = 0;
};


