#pragma once
//#include <memory>
#include <vector>
#include <string>
#include "Component.h"
#include "Physics.h"
#include "Appearence.h"
#include "Transform.h"
#include "BoundingSphere.h"
#include "ParticleData.h"

using namespace DirectX;
using namespace std;



class GameObject
{
public:
	GameObject(string type, initializer_list<Component*> componentList);

	~GameObject();

	XMMATRIX GetWorldMatrix() const { return XMLoadFloat4x4(&_world); }

	string GetType() const { return _objectType; }
	
	void SetParent(GameObject* _parent) { _parent = _parent; }

	void Update(float t);

	void SendMsg(Message message, Vector3 amount = Vector3());

	template <typename T>
	T GetComponent(ComponentType componentID)
	{
		for (auto comp : _components)
		{
			if (comp && comp->GetType() == componentID)
			{
				return static_cast<T>(comp);
			}
			//warning C4715: not all control paths return a value without the return nullptr
		}
		return nullptr;
	}

	void AddComponent(Component* newComponent);

private:
	GameObject* _parent;

	vector<Component*> _components;

	string _objectType;

	XMFLOAT4X4 _world;
};