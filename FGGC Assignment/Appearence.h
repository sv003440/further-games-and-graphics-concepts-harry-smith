#pragma once
#include <directxmath.h>
#include <d3d11_1.h>
#include "Component.h"

using namespace DirectX;
struct Geometry
{
	ID3D11Buffer * vertexBuffer;
	ID3D11Buffer * indexBuffer;
	int numberOfIndices;

	UINT vertexBufferStride;
	UINT vertexBufferOffset;
};

struct Material
{
	XMFLOAT4 diffuse;
	XMFLOAT4 ambient;
	XMFLOAT4 specular;
	float specularPower;
};

class Appearence : public Component
{
public:
	Appearence(Geometry geo, Material mat);
	~Appearence();

	void Draw(ID3D11DeviceContext * pImmediateContext);

	Geometry* GetGeometry() { return &_geometry; };
	Material GetMaterial() const { return _material; };
	
	void SetTextureRV(ID3D11ShaderResourceView * textureRV) { _textureRV = textureRV; }
	ID3D11ShaderResourceView * GetTextureRV() const { return _textureRV; }
	bool HasTexture() const { return _textureRV ? true : false; }

	void RecieveMsg(Message message, Vector3 amount = Vector3());

private:
	Geometry _geometry;
	Material _material;

	ID3D11ShaderResourceView * _textureRV;
};

