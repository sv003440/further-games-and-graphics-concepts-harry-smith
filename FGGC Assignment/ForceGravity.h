#pragma once
#include "ForceGenerator.h"

class ForceGravity : public ForceGenerator
{
public:
	ForceGravity(const Vector3 &gravity) :_gravity(gravity) {};
	~ForceGravity();

	void updateForce(GameObject* go) override;

private:
	Vector3 _gravity;
};

